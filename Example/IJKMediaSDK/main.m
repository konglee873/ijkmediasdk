//
//  main.m
//  IJKMediaSDK
//
//  Created by fe71f2f46be17aee340b7e5f44528bb0058000a1 on 07/21/2021.
//  Copyright (c) 2021 fe71f2f46be17aee340b7e5f44528bb0058000a1. All rights reserved.
//

@import UIKit;
#import "IJKMEDIASDKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IJKMEDIASDKAppDelegate class]));
    }
}
