//
//  IJKMEDIASDKViewController.m
//  IJKMediaSDK
//
//  Created by fe71f2f46be17aee340b7e5f44528bb0058000a1 on 07/21/2021.
//  Copyright (c) 2021 fe71f2f46be17aee340b7e5f44528bb0058000a1. All rights reserved.
//

#import "IJKMEDIASDKViewController.h"
#import <Masonry/Masonry.h>

#import <IJKMediaSDK/IJKMediaSDK.h>

typedef NS_ENUM(NSUInteger, ZBEAVPlayerBtnState) {
    ZBEAVPlayerBtnStatePlay = 1,
    ZBEAVPlayerBtnStatePause = 2,
};

@interface IJKMEDIASDKViewController ()

@property (nonatomic, strong, nullable) IJKFFMoviePlayerController *player;

@property (nonatomic, strong) UIView *playerView;

@property (nonatomic, strong) UIControl *playControl;

@property (nonatomic, strong) UISlider *slider;

@property (nonatomic, strong) id timeObserver;

@property (nonatomic, assign) NSTimeInterval playerPeriodicInterval;

@property (nonatomic, assign) BOOL isFirstFramePlay;

@property (nonatomic, assign) BOOL isSeeking;

@property (nonatomic, strong) UIButton *playBtn;

@property (nonatomic, assign) ZBEAVPlayerBtnState playState;

@property (nonatomic, strong, nullable) NSTimer *timer;

@property (nonatomic, assign) NSInteger idx;

@end

@implementation IJKMEDIASDKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupPlayerWithURL:@"rtmp://101.42.54.29/live/livestream?secret=edf6f3293b254390a92c1e7f8c21a9dc"];
    [self registerNoti];
    [self setupTimer];
}

- (void)setupUI {
    UIView *playerView = [[UIView alloc] initWithFrame:CGRectZero];
    playerView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:playerView];
    [playerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.playerView = playerView;

    UIControl *playControl = [[UIControl alloc] initWithFrame:CGRectZero];
    playControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    [self.view addSubview:playControl];
    [playControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [playControl addTarget:self action:@selector(playControlAction) forControlEvents:UIControlEventTouchUpInside];
    self.playControl = playControl;

    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(popAction) forControlEvents:UIControlEventTouchUpInside];
    [self.playControl addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.playControl).offset(10);
        make.top.equalTo(self.playControl).offset(24);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];

    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectZero];
    slider.minimumValue = 0.0;
    slider.maximumValue = 1.0;
    slider.minimumTrackTintColor = [UIColor lightGrayColor];
    [slider addTarget:self action:@selector(progressAction:) forControlEvents:UIControlEventValueChanged];
    [self.playControl addSubview:slider];
    self.slider = slider;
    [slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.playControl).offset(80);
        make.right.equalTo(self.playControl).offset(-80);
        make.bottom.equalTo(self.playControl).offset(-60);
        make.height.equalTo(@30);
    }];

    UIButton *playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBtn addTarget:self action:@selector(playBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.playControl addSubview:playBtn];
    self.playBtn = playBtn;
    [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.slider.mas_left).offset(-10);
        make.centerY.equalTo(self.slider.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(48, 48));
    }];
    self.playState = ZBEAVPlayerBtnStatePlay;

    UIButton *switchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    switchBtn.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    [switchBtn setTitle:@"Switch" forState:UIControlStateNormal];
    [switchBtn addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.playControl addSubview:switchBtn];
    [switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 50));
        make.right.equalTo(self.playControl).offset(-20);
        make.centerY.equalTo(backBtn.mas_centerY);
    }];

    UIButton *seekBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    seekBtn.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    [seekBtn setTitle:@"Seek" forState:UIControlStateNormal];
    [seekBtn addTarget:self action:@selector(seekAction) forControlEvents:UIControlEventTouchUpInside];
    [self.playControl addSubview:seekBtn];
    [seekBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 50));
        make.right.equalTo(switchBtn.mas_left).offset(-20);
        make.centerY.equalTo(backBtn.mas_centerY);
    }];
}

- (void)switchAction {
    ++self.idx;
    NSArray<NSString *> *urlArr = @[
        @"https://conan-online.fbcontent.cn/conan-english/15507292417026465.mp4",
        @"https://tutor-trans-video-online.fbcontent.cn/trans/178b46e9bf3e671-178b46e9c07d8e916b6bb65.mp4",
        @"https://conan-online.fbcontent.cn/conan-operation-resource/krx6fbvkj59qozg8hc.mp4",
        @"https://conan-online.fbcontent.cn/3192-HOTOLyck-hd.mp4",
        @"https://img-pub.fbcontent.cn/172e51b8ff1dfef.mp4",
        @"https://conan-online.fbcontent.cn/conan-xross-resource/zlc011emk/h3myaygpl.mp4",
        @"https://tutor-trans-video-online.fbcontent.cn/trans/1790cc186c29115-1790cc186d6a5d5558f45b5.mp4",
        @"https://img-pub.fbcontent.cn/173d71c362d04e6.mp4",
        @"https://conan-online.fbcontent.cn/10475-fMbOsVmW-sd.mp4",
        @"https://conan-online.fbcontent.cn/11515-agzsudaK-sd.mp4",
        @"https://conan-online.fbcontent.cn/11515-agzsudaK-sd.mp4",
        @"https://conan-online.fbcontent.cn/11511-XFGByuiK-sd.mp4",
    ];
    if (self.idx >= urlArr.count) {
        self.idx = 0;
    }
    [self switchWithURL:urlArr[self.idx]];
}

- (void)seekAction {

}

- (void)setupTimer {
    __weak typeof(self) weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        weakSelf.slider.value = weakSelf.player.currentPlaybackTime / weakSelf.player.duration;
    }];
}

- (void)invalidateTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)progressAction:(UISlider *)slider {
//    if (self.currentItem.status != AVPlayerStatusReadyToPlay) {
//        return;
//    }
    //1.slider的value值发生变化时，currentTime也在发生变化（在playerAction中定义了value的公式）
    float current = slider.value;
    //2.获取最终的currentTime
    float nextCurrent = current * self.player.duration;
    //3.拖动slider导致value的值改变时，player能够让正在进行的item追着时间走
    [self.player setCurrentPlaybackTime:nextCurrent];
//    [self.player seekToTime:CMTimeMakeWithSeconds(nextCurrent, 1.0)];
//    //6.使得计时器一直处于启动状态µc
//    self.timer.fireDate = [NSDate distantPast];
}

- (void)playBtnAction:(UIButton *)btn {
    if (self.playState == ZBEAVPlayerBtnStatePlay) {
        self.playState = ZBEAVPlayerBtnStatePause;
        [self pause];
    } else {
        self.playState = ZBEAVPlayerBtnStatePlay;
        [self play];
    }
}

- (void)switchWithURL:(NSString *)url {
    [self.player shutdown];
    [self.player.view removeFromSuperview];
    [self setupPlayerWithURL:url];
    [self.player pause];
    [self.player setCurrentPlaybackTime:30];
}

- (void)setPlayState:(ZBEAVPlayerBtnState)playState {
    _playState = playState;
    if (playState == ZBEAVPlayerBtnStatePlay) {
        [self.playBtn setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    } else {
        [self.playBtn setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    }
}

- (void)play {
    [self invalidateTimer];
    [self setupTimer];
    [self.player play];
}

- (void)pause {
    [self.player pause];
    [self invalidateTimer];
}


- (void)playControlAction {
    self.playControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.playControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    });
}

- (void)popAction {
    [self.player stop];
    self.player = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.player prepareToPlay];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.player shutdown];
}

- (void)dealloc {
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.player pause];
    [self invalidateTimer];
    self.timeObserver = nil;
}

- (void)setupPlayerWithURL:(NSString *)url {
    /// 高清 https://tutor-trans-video-online.fbcontent.cn/trans/179f5e7d66cab43-179f5e7d6837345430632be.mp4
    /// 标清 https://tutor-trans-video-online.fbcontent.cn/trans/179f5e7d632b28d-179f5e7d6497a8d6e2677eb.mp4
    /// 超清 https://img-pub.fbcontent.cn/179f5dc4eae38ae.mp4
//    NSString *sdURL = @"https://tutor-trans-video-online.fbcontent.cn/trans/179f5e7d632b28d-179f5e7d6497a8d6e2677eb.mp4";
//    NSString *hdURL = @"https://tutor-trans-video-online.fbcontent.cn/trans/179f5e7d66cab43-179f5e7d6837345430632be.mp4";
//    NSString *blueURL = @"https://img-pub.fbcontent.cn/179f5dc4eae38ae.mp4";
//    NSURL *originalURL = [NSURL URLWithString:hdURL];
//    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:originalURL resolvingAgainstBaseURL:NO];
//    urlComponents.scheme = ZBEMediaAssetURLScheme;
//    NSURL *tweakURL = urlComponents.URL;
//    AVURLAsset *asset = [AVURLAsset assetWithURL:tweakURL];
//    ZBEMediaAssetResourceLoader *resourceLoader = [[ZBEMediaAssetResourceLoader alloc] initWithOriginalURL:originalURL];
//    [asset.resourceLoader setDelegate:resourceLoader queue:dispatch_get_main_queue()];
//    objc_setAssociatedObject(asset, "resourceDelegate", resourceLoader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
//    self.currentItem = item;
//    self.player = [AVPlayer playerWithPlayerItem:item];
//    self.player.automaticallyWaitsToMinimizeStalling = NO;
//    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
//    self.playerLayer.frame = self.playerView.bounds;
//    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
//    [self.playerView.layer addSublayer:self.playerLayer];
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [[AVAudioSession sharedInstance] setActive:YES error:nil];

#ifdef DEBUG
    //debug日志模式
    [IJKFFMoviePlayerController setLogReport:YES];
    [IJKFFMoviePlayerController setLogLevel:k_IJK_LOG_DEBUG];
#else
    [IJKFFMoviePlayerController setLogReport:NO];
    [IJKFFMoviePlayerController setLogLevel:k_IJK_LOG_INFO];
#endif

    IJKFFOptions *options = [IJKFFOptions optionsByDefault];
    //开启硬件解码
//    [options setPlayerOptionIntValue:1 forKey:@"videotoolbox"];
    // 帧速率(fps) （可以改，确认非标准桢率会导致音画不同步，所以只能设定为15或者29.97）
//    [options setPlayerOptionIntValue:15 forKey:@"r"];
//    // 设置音量大小，256为标准音量。（要设置成两倍音量时则输入512，依此类推）
//    [options setPlayerOptionIntValue:512 forKey:@"vol"];
//    // 最大fps
//    [options setPlayerOptionIntValue:25 forKey:@"max-fps"];
    // 跳帧开关
//    [options setPlayerOptionIntValue:0 forKey:@"framedrop"];
    // 指定最大宽度
//    [options setPlayerOptionIntValue:960 forKey:@"videotoolbox-max-frame-width"];
//    // 自动转屏开关
//    [options setFormatOptionIntValue:0 forKey:@"auto_convert"];
    // 重连次数
//    [options setFormatOptionIntValue:1 forKey:@"reconnect"];

    /// 精准 seek
    [options setPlayerOptionIntValue:1 forKey:@"enable-accurate-seek"];

    // 超时时间
//    [options setFormatOptionIntValue:30 * 1000 forKey:@"timeout"];
    IJKFFMoviePlayerController *moviePlayer = [[IJKFFMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:url] withOptions:options];
    moviePlayer.scalingMode = IJKMPMovieScalingModeAspectFit;
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setPauseInBackground:YES];
    [self.playerView addSubview:moviePlayer.view];
    [moviePlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.playerView);
    }];
    self.player = moviePlayer;
    [self.player prepareToPlay];
}

- (BOOL)isLandscape {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersHomeIndicatorAutoHidden {
    return YES;
}

- (void)registerNoti {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadStateDidChange:)
                                                     name:IJKMPMoviePlayerLoadStateDidChangeNotification
                                                   object:self.player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:IJKMPMoviePlayerPlaybackDidFinishNotification
                                               object:self.player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:self.player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:IJKMPMoviePlayerPlaybackStateDidChangeNotification
                                               object:self.player];


}

- (void)loadStateDidChange:(NSNotification*)notification
{
    //    MPMovieLoadStateUnknown        = 0,
    //    MPMovieLoadStatePlayable       = 1 << 0,
    //    MPMovieLoadStatePlaythroughOK  = 1 << 1, // Playback will be automatically started in this state when shouldAutoplay is YES
    //    MPMovieLoadStateStalled        = 1 << 2, // Playback will be automatically paused in this state, if started

    IJKMPMovieLoadState loadState = self.player.loadState;

    if ((loadState & IJKMPMovieLoadStatePlaythroughOK) != 0 || (loadState & MPMovieLoadStatePlayable) != 0) {
        //缓冲完成
        NSLog(@"loadStateDidChange: IJKMPMovieLoadStatePlaythroughOK: %d\n", (int)loadState);

    }
    else if ((loadState & IJKMPMovieLoadStateStalled) != 0) {
        //缓冲开始
        NSLog(@"loadStateDidChange: IJKMPMovieLoadStateStalled: %d\n", (int)loadState);
    }
    else {
        NSLog(@"loadStateDidChange: ???: %d\n", (int)loadState);
    }
}

- (void)moviePlayBackDidFinish:(NSNotification*)notification
{
    //    MPMovieFinishReasonPlaybackEnded,
    //    MPMovieFinishReasonPlaybackError,
    //    MPMovieFinishReasonUserExited
    int reason = [[[notification userInfo] valueForKey:IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    switch (reason)
    {
        case IJKMPMovieFinishReasonPlaybackEnded:
            NSLog(@"playbackStateDidChange: IJKMPMovieFinishReasonPlaybackEnded: %d\n", reason);

            break;

        case IJKMPMovieFinishReasonUserExited:
            NSLog(@"playbackStateDidChange: IJKMPMovieFinishReasonUserExited: %d\n", reason);
            break;

        case IJKMPMovieFinishReasonPlaybackError:
            NSLog(@"playbackStateDidChange: IJKMPMovieFinishReasonPlaybackError: %d\n", reason);
            break;

        default:
            NSLog(@"playbackPlayBackDidFinish: ???: %d\n", reason);
            break;
    }
}

- (void)moviePlayBackStateDidChange:(NSNotification*)notification
{
    //    MPMoviePlaybackStateStopped,
    //    MPMoviePlaybackStatePlaying,
    //    MPMoviePlaybackStatePaused,
    //    MPMoviePlaybackStateInterrupted,
    //    MPMoviePlaybackStateSeekingForward,
    //    MPMoviePlaybackStateSeekingBackward

    switch (self.player.playbackState)
    {
        case IJKMPMoviePlaybackStateStopped: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: stoped", (int)self.player.playbackState);
            break;
        }
        case IJKMPMoviePlaybackStatePlaying: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: playing", (int)self.player.playbackState);
            break;
        }
        case IJKMPMoviePlaybackStatePaused: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: paused", (int)self.player.playbackState);
            break;
        }
        case IJKMPMoviePlaybackStateInterrupted: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: interrupted", (int)self.player.playbackState);
            break;
        }
        case IJKMPMoviePlaybackStateSeekingForward:
        case IJKMPMoviePlaybackStateSeekingBackward: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: seeking", (int)self.player.playbackState);
            break;
        }
        default: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: unknown", (int)self.player.playbackState);
            break;
        }
    }
}

- (void)mediaIsPreparedToPlayDidChange:(NSNotification *)noti {
    if (!self.player.isPlaying) {
        [self.player play];
    }
    NSLog(@"%s", __FUNCTION__);
}


@end
