#
# Be sure to run `pod lib lint IJKMediaSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IJKMediaSDK'
  s.version          = '0.1.0'
  s.summary          = 'A short description of IJKMediaSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/fe71f2f46be17aee340b7e5f44528bb0058000a1/IJKMediaSDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'fe71f2f46be17aee340b7e5f44528bb0058000a1' => 'liqiangbj01@yuanfudao.com' }
  s.source           = { :git => 'https://github.com/fe71f2f46be17aee340b7e5f44528bb0058000a1/IJKMediaSDK.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.subspec 'IJKMediaPlayer' do |media|
    media.source_files = [
      'IJKMediaSDK/Classes/IJKMediaPlayer/*.{h,m,c,mm}',
    ]
    media.public_header_files = [
      'IJKMediaSDK/Classes/IJKMediaPlayer/IJKKVOController.h',
      'IJKMediaSDK/Classes/IJKMediaPlayer/IJKMediaModule.h',
      'IJKMediaSDK/Classes/IJKMediaPlayer/IJKMediaPlayback.h',
      'IJKMediaSDK/Classes/IJKMediaPlayer/IJKMediaPlayer.h',
      'IJKMediaSDK/Classes/IJKMediaPlayer/IJKNotificationManager.h'
    ]
    media.requires_arc = true
  end

  s.subspec 'IJKAVMoviePlayerController' do |av|
    av.source_files = [
      'IJKMediaSDK/Classes/IJKAVMoviePlayerController/*.{h,m,c,mm}',
    ]
    av.public_header_files = [
      'IJKMediaSDK/Classes/IJKAVMoviePlayerController/IJKAVMoviePlayerController.h',
    ]
    av.requires_arc = true
  end

  s.subspec 'IJKMPMoviePlayerController' do |mp|
    mp.source_files = [
      'IJKMediaSDK/Classes/IJKMPMoviePlayerController/*.{h,m,c,mm}',
    ]
    mp.public_header_files = [
      'IJKMediaSDK/Classes/IJKMPMoviePlayerController/IJKMPMoviePlayerController.h',
    ]
    mp.requires_arc = true
  end

  s.subspec 'ijkmedia' do |ijk|
    ijk.source_files = [
      'IJKMediaSDK/Classes/ijkmedia/**/*',
    ]
    ijk.header_dir = 'ijkmedia'
    ijk.header_mappings_dir = 'IJKMediaSDK/Classes/ijkmedia/**'
    ijk.vendored_libraries = 'IJKMediaSDK/Classes/ffmpeg/lib/*.a'
    ijk.requires_arc = true
    ijk.private_header_files = [
      'IJKMediaSDK/Classes/ijkmedia/**/*',
    ]
  end

  s.source_files = [
    'IJKMediaSDK/Classes/IJKFFMoviePlayerController/**/*.{h,m,c,mm}',
    'IJKMediaSDK/IJKMediaSDK.h',
  ]

  s.public_header_files = [
    'IJKMediaSDK/IJKMediaSDK.h',
    'IJKMediaSDK/Classes/IJKFFMoviePlayerController/ARC/IJKFFMonitor.h',
    'IJKMediaSDK/Classes/IJKFFMoviePlayerController/ARC/IJKFFMoviePlayerController.h',
    'IJKMediaSDK/Classes/IJKFFMoviePlayerController/ARC/IJKFFOptions.h',
    'IJKMediaSDK/Classes/IJKFFMoviePlayerController/ARC/IJKSDLGLViewProtocol.h'
  ]
  s.requires_arc = false
  s.requires_arc = 'IJKMediaSDK/Classes/IJKFFMoviePlayerController/ARC/**/*.{h,m,c,mm}'
  s.frameworks = 'AudioToolbox', 'AVFoundation', 'CoreGraphics', 'CoreMedia', 'CoreVideo', 'MediaPlayer', 'MobileCoreServices', 'OpenGLES', 'QuartzCore', 'UIKit', 'VideoToolbox'
  s.library = 'z', 'stdc++', 'bz2'

end

