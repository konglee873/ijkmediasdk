# IJKMediaSDK

[![CI Status](https://img.shields.io/travis/fe71f2f46be17aee340b7e5f44528bb0058000a1/IJKMediaSDK.svg?style=flat)](https://travis-ci.org/fe71f2f46be17aee340b7e5f44528bb0058000a1/IJKMediaSDK)
[![Version](https://img.shields.io/cocoapods/v/IJKMediaSDK.svg?style=flat)](https://cocoapods.org/pods/IJKMediaSDK)
[![License](https://img.shields.io/cocoapods/l/IJKMediaSDK.svg?style=flat)](https://cocoapods.org/pods/IJKMediaSDK)
[![Platform](https://img.shields.io/cocoapods/p/IJKMediaSDK.svg?style=flat)](https://cocoapods.org/pods/IJKMediaSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IJKMediaSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IJKMediaSDK'
```

## Author

fe71f2f46be17aee340b7e5f44528bb0058000a1, liqiangbj01@yuanfudao.com

## License

IJKMediaSDK is available under the MIT license. See the LICENSE file for more info.
